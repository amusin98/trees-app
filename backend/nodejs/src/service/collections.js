const mongoose = require('mongoose');
const collectionsModel = mongoose.model('Collection');

module.exports.getAll = () => {
  return collectionsModel.find()
};

module.exports.getById = (id) => {
  return collectionsModel.findOne({_id: id})
};

module.exports.insert = (collection) => {
  return collectionsModel.create(collection)
};

module.exports.update = (nodeId, collection) => {
  return new Promise((resolve, reject) => {
    collectionsModel.deleteOne({_id: nodeId})
      .then(() => {
        collectionsModel.create(collection)
          .then(insertedCollection => {
            collectionsModel.find({parent: nodeId})
              .then(collections => {
                Promise.all(collections.map(item => {
                  item.parent = insertedCollection._id;
                  return item.save()
                    .catch(e => Promise.reject(e));
                }))
                  .then(() => resolve(insertedCollection))
                  .catch(e => reject(e));
              })
          })
          .catch(e => reject(e));
      })
      .catch(e => reject(e));
  })
};

module.exports.updateMetadata = (id, metadata) => {
  return new Promise((resolve, reject) => {
    collectionsModel.findOne({_id: id})
      .then(collection => {
        collection.metadata = metadata;
        collection.save()
          .then(collection => resolve(collection))
          .catch(e => reject(e))
      })
      .catch(e => reject(e));
  })
};


const deleteRecursive = (id) => {
  return new Promise((resolve, reject) => {
    collectionsModel.deleteOne({_id: id})
      .then(() => {
        collectionsModel.find({parent: id})
          .then(collections => {
            Promise.all(collections.map(item => deleteRecursive(item._id)))
              .then(() => resolve())
              .catch(() => reject());
          })
          .catch(e => reject(e));
      })
      .catch(e => reject(e));
  })
};

module.exports.delete = (id) => {
  return deleteRecursive(id)
};