const collectionService = require('../service/collections');

module.exports.getAll = (req, res) => {
  return collectionService.getAll()
    .then(collections => sendJsonResponse(res, 200, collections))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Not found, cant get all collections',
    }));
};

module.exports.insert = (req, res) => {
  collectionService.insert({...req.body})
    .then(insertedCollection => sendJsonResponse(res, 201, insertedCollection))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Cant insert node',
    }));
};

module.exports.update = (req, res) => {
  if (!req.params.id) {
    sendJsonResponse(res, 404, {
      "message": 'Not found, collection id is required',
    });
  }
  collectionService.update(req.params.id, {...req.body})
    .then(updatedCollection => sendJsonResponse(res, 200, updatedCollection))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Cant update node',
    }))
};

module.exports.updateMetadata = (req, res) => {
  if (!req.params.id) {
    sendJsonResponse(res, 404, {
      "message": 'Not found, id is required',
    });
  }
  collectionService.updateMetadata(req.params.id, req.body.metadata)
    .then(collection => sendJsonResponse(res, 200, collection))
    .catch(e => sendJsonResponse(res, 400, {
      "message": 'Cant update node',
    }))
};

module.exports.delete = (req, res) => {
  if (!req.params.id) {
    sendJsonResponse(res, 404, {
      "message": 'Not found, id is required',
    });
  }
  collectionService.delete(req.params.id)
    .then(() => sendJsonResponse(res, 200, {}))
    .catch(() => sendJsonResponse(res, 400, {
      "message": 'Cant update node',
    }))
};

sendJsonResponse = (res, status, content) => {
  res.status(status);
  res.json(content);
};