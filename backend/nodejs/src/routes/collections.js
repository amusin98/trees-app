const express = require('express');
const router = express.Router();
const collectionsController = require('../controllers/collections');

router.get('/collections', collectionsController.getAll);
router.post('/collections', collectionsController.insert);
router.put('/collections/:id', collectionsController.update);
router.put('/collections/:id/meta', collectionsController.updateMetadata);
router.delete('/collections/:id', collectionsController.delete);


module.exports = router;
