package by.trees.trees_api.service;

import by.trees.trees_api.model.Collection;
import by.trees.trees_api.model.CollectionsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class CollectionsService {

    @Autowired
    private CollectionsDAO dao;

    public Optional<Collection> insert(Collection collection) {
        boolean result = dao.insert(collection);
        return result ? Optional.of(collection) : Optional.empty();
    }

    public boolean delete(String id) {
        return dao.delete(id);
    }

    public List<Collection> getAll() {
        System.out.println("2");
        return dao.getAll();
    }

    public Optional<Collection> update(String id, Collection collection) {
        boolean result = dao.update(id, collection);
        return result ? Optional.of(collection) : Optional.empty();
    }

    public Optional<Collection> updateMeta(String id, Map metadata) {
        boolean result = dao.updateMeta(id, metadata);
        return result ? Optional.of(dao.getById(id)) : Optional.empty();
    }
}
