package by.trees.trees_api.model;

import by.trees.trees_api.model.mapper.CollectionMapper;
import com.mongodb.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
public class CollectionsDAO {

    @Autowired
    private CollectionMapper mapper;

    @Autowired
    private DBCollection collections;

    public List<Collection> getAll() {
        System.out.println("3");
        List<Collection> collectionsResponse = new ArrayList<>();
        DBCursor dbObjects = collections.find();
        dbObjects.forEach(dbObject -> collectionsResponse.add(mapper.map(dbObject)));
        dbObjects.close();
        return collectionsResponse;
    }

    public Collection getById(String id) {
        return mapper.map(collections.findOne(new BasicDBObject("_id", id)));
    }

    public boolean updateMeta(String id, Map meta) {
        DBObject object = collections.findOne(new BasicDBObject("_id", id));
        object.putAll(meta);
        WriteResult save = collections.save(object);
        return save.getN() == 1;
    }

    public boolean delete(String id) {
        WriteResult result = collections.remove(new BasicDBObject("_id", id));
        if (result.getN() != 1) {
            return false;
        }
        DBCursor childs = collections.find(new BasicDBObject().append("parent", id));
        for (DBObject child : childs) {
            if (!delete((String) child.get("_id"))) {
                return false;
            }
        }
        childs.close();
        return true;
    }

    public boolean update(String id, Collection collection) {
        WriteResult result = collections.remove(new BasicDBObject("_id", id));
        if (result.getN() != 1) {
            return false;
        }
        if(!insert(collection)) {
            return false;
        }
        DBCursor childs = collections.find(new BasicDBObject().append("parent", id));
        for (DBObject child : childs) {
            child.put("parent", collection.get_id());
            WriteResult update = collections.update(new BasicDBObject("_id", child.get("_id")), child);
            if (update.getN() != 1) {
                return false;
            }
        }
        childs.close();
        return true;
    }


    public boolean insert(Collection collection) {
        WriteResult save = collections.save(mapper.map(collection));
        return save.getN() == 1;
    }

}
