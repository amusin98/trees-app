package by.trees.trees_api.controller;

import by.trees.trees_api.model.Collection;
import by.trees.trees_api.service.CollectionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/api/collections")
public class CollectionsController {

    @Autowired
    private CollectionsService service;

    @GetMapping
    public ResponseEntity<List<Collection>> getAll() {
        return new ResponseEntity<>(service.getAll(), HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection> create(@RequestBody Collection node) {
        Optional<Collection> newNode = service.insert(node);
        return newNode.map(collection -> new ResponseEntity<>(collection, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PutMapping(value = "/{id}/meta")
    public ResponseEntity<Collection> updateMeta(@PathVariable String id, @RequestBody Map metadata) {
        Optional<Collection> updatedNode = service.updateMeta(id, metadata);
        return updatedNode.map(collection -> new ResponseEntity<>(collection, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @PutMapping(value = "/{id}")
    public ResponseEntity<Collection> update(@PathVariable String id, @RequestBody Collection node) {
        Optional<Collection> updatedNode = service.update(id, node);
        return updatedNode.map(collection -> new ResponseEntity<>(collection, HttpStatus.CREATED)).orElseGet(() -> new ResponseEntity<>(HttpStatus.BAD_REQUEST));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable String id) {
        boolean result = service.delete(id);
        if (result) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}
