package by.trees.trees_api.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
public class Collection {
    @NonNull
    String _id;
    String parent;
    Map metadata;
}
