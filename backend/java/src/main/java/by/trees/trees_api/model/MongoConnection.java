package by.trees.trees_api.model;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.WriteConcern;
import lombok.extern.slf4j.Slf4j;

import java.net.UnknownHostException;

@Slf4j
public class MongoConnection {

    private MongoClient mongoClient;

    private static MongoConnection connection = new MongoConnection();

    private MongoConnection(){}

    public MongoClient getMongoClient() {
        if (mongoClient == null) {
            log.debug("Start mongo");
            MongoClientOptions.Builder options = MongoClientOptions.builder()
                    .connectionsPerHost(10)
                    .maxConnectionIdleTime((60 * 1_000))
                    .maxConnectionLifeTime((120 * 1_000));

            MongoClientURI mongoClientURI = new MongoClientURI("mongodb://localhost:27017", options);

            log.info("About connect to mongo db: " + mongoClientURI);

            try {
                mongoClient = new MongoClient(mongoClientURI);
                mongoClient.setWriteConcern(WriteConcern.ACKNOWLEDGED);
            } catch (UnknownHostException e) {
                log.error(e.getMessage(), e);
            }
            mongoClient.setWriteConcern(WriteConcern.ACKNOWLEDGED);
        }
        return  mongoClient;
    }

    public void init() {
        log.debug("Bootstrapping");
        getMongoClient();
    }

    public void close() {
        log.info("Closing mongo connection");
        if (mongoClient != null) {
            mongoClient.close();
            mongoClient = null;
        }
    }

    public static MongoConnection getInstance() {
        return connection;
    }

}
