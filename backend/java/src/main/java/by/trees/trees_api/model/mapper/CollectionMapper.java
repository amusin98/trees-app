package by.trees.trees_api.model.mapper;

import by.trees.trees_api.model.Collection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class CollectionMapper {
    public Collection map(DBObject dbObject) {
        return new Collection((String) dbObject.get("_id"), (String) dbObject.get("parent"), (Map) dbObject.get("metadata"));
    }

    public DBObject map(Collection collection) {
        return new BasicDBObject("_id", collection.get_id())
                .append("parent", collection.getParent())
                .append("metadata", collection.getMetadata());
    }
}
