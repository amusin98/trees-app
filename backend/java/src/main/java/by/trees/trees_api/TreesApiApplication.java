package by.trees.trees_api;

import by.trees.trees_api.model.MongoConnection;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.EventListener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

@SpringBootApplication
public class TreesApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TreesApiApplication.class, args);
    }

    @Bean
    protected ServletContextListener listener() {
        return new ServletContextListener() {
            @Override
            public void contextInitialized(ServletContextEvent sce) {
                MongoConnection mongoConnection = MongoConnection.getInstance();
                mongoConnection.init();
                System.out.println("started");
            }

            @Override
            public void contextDestroyed(ServletContextEvent sce) {
                System.out.println("closed");
                MongoConnection mongoConnection = MongoConnection.getInstance();
                mongoConnection.close();

            }
        };
    }

    @Bean
    public DBCollection collectionRepository() {
        MongoConnection instance = MongoConnection.getInstance();
        MongoClient mongoClient = instance.getMongoClient();
        DB collections_db = mongoClient.getDB("collections_db");
        return collections_db.getCollection("collections");
    }


}
