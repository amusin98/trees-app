package by.trees.trees_api.controller;

import by.trees.trees_api.model.Collection;
import com.google.common.collect.ImmutableMap;
import org.springframework.http.HttpStatus;
import org.testng.annotations.DataProvider;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

public class CollectionControllerTestData {
    @DataProvider(name = "getAll")
    public static Object[][] getAllData() {
        return new Object[][] {
                {Arrays.asList(
                        new Collection("node1", "parent1", Collections.emptyMap()),
                        new Collection("node2", "parent2", Collections.emptyMap())),
                        HttpStatus.OK},
                {Collections.emptyList(), HttpStatus.OK}
        };
    }

    @DataProvider(name = "insert")
    public static Object[][] insertData() {
        Collection mock = new Collection("node1", "parent1", Collections.emptyMap());
        return new Object[][] {
                {mock, Optional.of(mock), HttpStatus.CREATED},
                {mock, Optional.empty(), HttpStatus.BAD_REQUEST}
        };
    }

    @DataProvider(name = "update")
    public static Object[][] updateData() {
        Collection mock = new Collection("node1", "parent1", Collections.emptyMap());
        return new Object[][] {
                {"oldNodeId", mock, Optional.of(mock), HttpStatus.CREATED},
                {"oldNodeId", mock, Optional.empty(), HttpStatus.BAD_REQUEST}
        };
    }

    @DataProvider(name = "updateMeta")
    public static Object[][] updateMetadataData() {
        Collection mock = new Collection("node1", "parent1", Collections.emptyMap());
        return new Object[][] {
                {"oldNodeId", ImmutableMap.of("name", "value"), Optional.of(mock), HttpStatus.CREATED},
                {"oldNodeId", ImmutableMap.of("name", "value"), Optional.empty(), HttpStatus.BAD_REQUEST}
        };
    }

    @DataProvider(name = "delete")
    public static Object[][] deleteData() {
        return new Object[][] {
                {"nodeId", true, HttpStatus.NO_CONTENT},
                {"nodeId", false, HttpStatus.BAD_REQUEST}
        };
    }
}
