package by.trees.trees_api.model.mapper;

import by.trees.trees_api.model.Collection;
import com.google.common.collect.ImmutableMap;
import com.mongodb.BasicDBObject;
import org.testng.annotations.DataProvider;

import java.util.Collections;

public class MapperTestData {
    @DataProvider(name = "fromCollectionToDBObject")
    public static Object[][] collections() {
        return new Object[][]{
                {
                        new Collection("testId", "testParent", Collections.emptyMap()),
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", Collections.emptyMap())
                },
                {
                        new Collection("testId", null, Collections.emptyMap()),
                        new BasicDBObject("_id", "testId")
                                .append("parent", null)
                                .append("metadata", Collections.emptyMap())
                },
                {
                        new Collection("testId", "testParent", ImmutableMap.of("name", "node name", "description", "node description")),
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", new BasicDBObject()
                                .append("name", "node name")
                                .append("description", "node description"))
                }
        };
    }

    @DataProvider(name = "fromDBObjectToCollection")
    public static Object[][] dbObjects() {
        return new Object[][]{
                {
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", Collections.emptyMap()),
                        new Collection("testId", "testParent", Collections.emptyMap()),
                },
                {
                        new BasicDBObject("_id", "testId")
                                .append("parent", null)
                                .append("metadata", Collections.emptyMap()),
                        new Collection("testId", null, Collections.emptyMap())
                },
                {
                        new BasicDBObject("_id", "testId")
                                .append("parent", "testParent")
                                .append("metadata", new BasicDBObject()
                                .append("name", "node name")
                                .append("description", "node description")),
                        new Collection("testId", "testParent", ImmutableMap.of("name", "node name", "description", "node description")),
                }
        };
    }
}
