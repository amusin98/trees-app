package by.trees.trees_api.service;

import by.trees.trees_api.TreesApiApplication;
import by.trees.trees_api.model.Collection;
import by.trees.trees_api.model.CollectionsDAO;
import com.google.common.collect.ImmutableMap;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.testng.Assert.*;

@TestExecutionListeners(MockitoTestExecutionListener.class)
@SpringBootTest
@ContextConfiguration(classes = TreesApiApplication.class)
public class CollectionsServiceTest extends AbstractTestNGSpringContextTests {

    @InjectMocks
    @Autowired
    private CollectionsService service;

    @MockBean
    private CollectionsDAO dao;



    @Test(dataProvider = "collectionsInsert", dataProviderClass = CollectionServiceTestData.class)
    public void testInsert(Collection collection, boolean result) {
        given(dao.insert(collection)).willReturn(result);
        Optional<Collection> insertResult = service.insert(collection);
        if (result) {
            assertEquals(insertResult, Optional.of(collection));
        } else {
            assertEquals(insertResult, Optional.empty());
        }
    }



    @Test(dataProvider = "deleteProvider", dataProviderClass = CollectionServiceTestData.class)
    public void testDelete(String id, boolean result) {
        given(dao.delete(id)).willReturn(result);
        boolean deleteResult = service.delete(id);
        assertEquals(deleteResult, result);
    }



    @Test(dataProvider = "collectionsGet", dataProviderClass = CollectionServiceTestData.class)
    public void testGetAll(List<Collection> collections) {
        System.out.println("1");
        given(dao.getAll()).willReturn(collections);
        List<Collection> result = service.getAll();

        assertEquals(result, collections);


    }




    @Test(dataProvider = "updateProvider", dataProviderClass = CollectionServiceTestData.class)
    public void testUpdate(String id, Collection collection, boolean result) {
        given(dao.update(id, collection)).willReturn(result);
        Optional<Collection> updateResult = service.update(id, collection);
        if (result) {
            assertEquals(updateResult, Optional.of(collection));
        } else {
            assertEquals(updateResult, Optional.empty());
        }
    }



    @Test(dataProvider = "updateMetaProvider", dataProviderClass = CollectionServiceTestData.class)
    public void testUpdateMeta(String id, Map metadata, boolean result) {
        Collection mockCollection = new Collection(id, null, metadata);
        given(dao.updateMeta(id, metadata)).willReturn(result);
        given(dao.getById(id)).willReturn(mockCollection);
        Optional<Collection> collection = service.updateMeta(id, metadata);
        if (result) {
            assertEquals(collection, Optional.of(mockCollection));
        } else {
            assertEquals(collection, Optional.empty());
        }
    }
}