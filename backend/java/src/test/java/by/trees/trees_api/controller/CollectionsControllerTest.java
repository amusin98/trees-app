package by.trees.trees_api.controller;

import by.trees.trees_api.TreesApiApplication;
import by.trees.trees_api.model.Collection;
import by.trees.trees_api.service.CollectionsService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.Test;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.testng.Assert.*;

@TestExecutionListeners(MockitoTestExecutionListener.class)
@WebMvcTest(CollectionsController.class)
@ContextConfiguration(classes = TreesApiApplication.class)
public class CollectionsControllerTest extends AbstractTestNGSpringContextTests {


    @Autowired
    MockMvc mockMvc;

    @MockBean
    private CollectionsService service;


    @Test(dataProvider = "getAll", dataProviderClass = CollectionControllerTestData.class)
    public void testGetAll(List<Collection> response, HttpStatus status) throws Exception {
        given(service.getAll()).willReturn(response);
        mockMvc.perform(get("/api/collections"))
                .andExpect(status().is(status.value()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$", hasSize(response.size())));


    }

    @Test(dataProvider = "insert", dataProviderClass = CollectionControllerTestData.class)
    public void testCreate(Collection collection, Optional<Collection> response, HttpStatus status) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writeValueAsString(collection);
        given(service.insert(collection)).willReturn(response);
        if (response.isPresent()) {
            mockMvc.perform(post("/api/collections")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$._id", is(response.get().get_id())))
                    .andExpect(jsonPath("$.parent", is(response.get().getParent())))
                    .andExpect(jsonPath("$.metadata", is(response.get().getMetadata())));
        } else {
            mockMvc.perform(post("/api/collections")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()));
        }
    }

    @Test(dataProvider = "updateMeta", dataProviderClass = CollectionControllerTestData.class)
    public void testUpdateMeta(String id, Map metadata, Optional<Collection> response, HttpStatus status) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writeValueAsString(metadata);
        given(service.updateMeta(id, metadata)).willReturn(response);
        if (response.isPresent()) {
            mockMvc.perform(put("/api/collections/{id}/meta", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$._id", is(response.get().get_id())))
                    .andExpect(jsonPath("$.parent", is(response.get().getParent())))
                    .andExpect(jsonPath("$.metadata", is(response.get().getMetadata())));
        } else {
            mockMvc.perform(put("/api/collections/{id}/meta", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()));
        }
    }

    @Test(dataProvider = "update", dataProviderClass = CollectionControllerTestData.class)
    public void testUpdate(String id, Collection collection, Optional<Collection> response, HttpStatus status) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String body = mapper.writeValueAsString(collection);
        given(service.update(id, collection)).willReturn(response);
        if (response.isPresent()) {
            mockMvc.perform(put("/api/collections/{id}", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                    .andExpect(jsonPath("$._id", is(response.get().get_id())))
                    .andExpect(jsonPath("$.parent", is(response.get().getParent())))
                    .andExpect(jsonPath("$.metadata", is(response.get().getMetadata())));
        } else {
            mockMvc.perform(put("/api/collections/{id}", id)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(body))
                    .andExpect(status().is(status.value()));
        }
    }

    @Test(dataProvider = "delete", dataProviderClass = CollectionControllerTestData.class)
    public void testDelete(String id, boolean result, HttpStatus status) throws Exception {
        given(service.delete(id)).willReturn(result);
        mockMvc.perform(delete("/api/collections/{id}", id))
                .andExpect(status().is(status.value()));
    }
}