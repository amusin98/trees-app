package by.trees.trees_api.model.mapper;

import by.trees.trees_api.TreesApiApplication;
import by.trees.trees_api.model.Collection;
import com.mongodb.BasicDBObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@ContextConfiguration(classes = TreesApiApplication.class)
public class CollectionMapperTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private CollectionMapper mapper;



    @Test(dataProvider = "fromCollectionToDBObject", dataProviderClass = MapperTestData.class)
    public void testMap(Collection input, BasicDBObject output) {
        assertEquals(mapper.map(input), output);
    }

    @Test(dataProvider = "fromDBObjectToCollection", dataProviderClass = MapperTestData.class)
    public void testMap1(BasicDBObject input, Collection output) {
        Collection result = mapper.map(input);
        assertEquals(result.getMetadata().keySet(), output.getMetadata().keySet());
        assertEquals(result.getMetadata().values(), output.getMetadata().values());
        assertEquals(result.get_id(), output.get_id());
        assertEquals(result.getParent(), output.getParent());
    }
}