const config = ($routeProvider) => {
  $routeProvider
    .when('/', {
      template: '<trees-page></trees-page>'
    })
    .otherwise('/');
};
config.$inject = ['$routeProvider'];

export default config;