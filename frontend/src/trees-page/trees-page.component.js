import template from './trees-page.template.html';
import './trees-page.less';
import API from './TreesAPI';
import toast from '../toast/toast-service';

function mainPartController($http, ngToast) {
  let vm = this;
  let client = new API($http);
  client.getAll()
    .then(response => {
      vm.collections = response.data;
      vm.currentNode = vm.collections.find(collection => collection.parent === null);
      vm.roots = vm.collections.filter(collection => collection.parent === null);
    })
    .catch(e => toast.error(ngToast, e));

  vm.changeCurrentNodeCallback = function (nodeId) {
    vm.currentNode = vm.collections.find(collection => collection._id === nodeId);
  };

  vm.changeMetadataCallback = (metadata) => {
    client.updateMeta(vm.currentNode._id, {metadata: JSON.parse(metadata)})
      .then(() => vm.collections
        .find(collection => collection._id === this.currentNode._id).metadata = JSON.parse(metadata))
      .catch(e => toast.error(ngToast, e));
  };

  vm.addCollectionCallback = function (name) {
    client.insert({
      _id: name,
      parent: null,
      metadata: {}
    })
      .then((resp) => {
        vm.collections.push(resp.data);
        vm.roots.push(resp.data);
      })
      .catch(e => toast.error(ngToast, e));
  };

  vm.addNodeCallback = (name) => {
    client.insert({
      _id: name,
      parent: vm.currentNode._id,
      metadata: {}
    })
      .then(() => {
        vm.collections.push({_id: name, parent: vm.currentNode._id, metadata: {}});
        vm.roots = vm.collections.filter(collection => collection.parent === null);
      })
      .catch(e => toast.error(ngToast, e));
  };

  vm.updateNodeCallback = (name) => {
    client.update(vm.currentNode._id, {
      _id: name,
      parent: vm.currentNode.parent,
      metadata: vm.currentNode.metadata,
    })
      .then(() => {
        vm.collections
          .filter(collection => collection.parent === vm.currentNode._id)
          .map(collection => collection.parent = name);
        vm.collections.find(collection => collection._id === vm.currentNode._id)._id = name;
        vm.roots = vm.collections.filter(collection => collection.parent === null);
      })
      .catch(e => toast.error(ngToast, e));
  };

  vm.deleteNodeCallback = (node) => {
    client.delete(vm.currentNode._id)
      .then(() => {
        recursiveRemoving(node);
        vm.roots = vm.collections.filter(collection => collection.parent === null);
        vm.currentNode = vm.roots[0];
      })
      .catch(e => toast.error(ngToast, e));
  };

  const recursiveRemoving = (node) => {
    if (vm.collections.some(collection => collection.parent === node._id)) {
      let childs = vm.collections.filter(collection => collection.parent === node._id);
      vm.collections = vm.collections
        .filter(collection => collection.parent !== node._id && collection._id !== node._id);
      childs.forEach(child => recursiveRemoving(child));
    }
    vm.collections = vm.collections.filter(collection => collection._id !== node._id);
  };
}

export default {
  template,
  controller: ['$http', 'ngToast', mainPartController],
};