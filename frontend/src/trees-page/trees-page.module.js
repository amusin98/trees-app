import angular from 'angular';
import component from './trees-page.component';
import trees from '../tree-list/tree-list.module';
import nodeMetadata from '../node-metadata/node-metadata.module';
import headerModule from '../header/header.module';
import footerModule from '../footer/footer.module';


export default angular.module('treesPageModule', [
  headerModule,
  trees,
  nodeMetadata,
  footerModule,
  'ngToast',
])
  .component('treesPage', component)
  .name;