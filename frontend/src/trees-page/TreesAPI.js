export default class TreesAPI {
  constructor(http) {
    this.client = http;
  }

  getAll() {
    return this.client.get('/api/collections');
  }

  insert(node) {
    return this.client.post(`/api/collections`, JSON.stringify(node));
  }

  update(nodeId, newNode) {
    return this.client.put(`/api/collections/${nodeId}`, JSON.stringify(newNode));
  }

  updateMeta(nodeId, meta) {
    return this.client.put(`/api/collections/${nodeId}/meta`, meta);
  }

  delete(nodeId) {
    return this.client.delete(`/api/collections/${nodeId}`);
  }

}