import template from './header.template.html';
import './header.less';

export default {
  bindings: {
    addCollectionCallback: '&',
    addNodeCallback: '&',
    deleteNodeCallback: '&',
    updateNodeCallback: '&',
    currentNode: '<',
  },
  template,
};
