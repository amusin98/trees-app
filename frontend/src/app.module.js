import angular from 'angular';
import ngRoute from 'angular-route';
import config from './app.config';
import treesPageModule from './trees-page/trees-page.module';
import ngSanitize from 'angular-sanitize';
import ngAnimate from 'angular-animate';
import ngToast from 'ng-toast';
import 'ng-toast/dist/ngToast.min.css';
import 'bootstrap/dist/css/bootstrap.min.css';

angular
  .module('myApp', [ngRoute, treesPageModule, 'ngToast'])
  .config(config);