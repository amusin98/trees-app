import angular from 'angular';
import component from './tree-list.component';

export default angular.module('treeList', [])
  .component('treeList', component)
  .name;