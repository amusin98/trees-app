import template from './tree-list.template.html';
import './tree-list.less';

export default {
  template,
  bindings: {
    collections: '<',
    currentNode: '<',
    changeCurrentNodeCallback: '&',
    roots: '<'
  },
  controller: function treesController() {
    let vm = this;
    vm.childs = [];
    vm.childsShown = [];

    vm.$onChanges = function () {
      if (vm.collections && vm.roots) {
        vm.roots
          .forEach(collection => vm.childs[collection._id] = vm.collections
            .filter(item => item.parent === collection._id));
      }
    };

    vm.haveChilds = (nodeId) => {
      return vm.childs[nodeId] && vm.childs[nodeId].length;
    };

    vm.showChilds = (nodeId) => {
      vm.childsShown[nodeId] = !vm.childsShown[nodeId];
    };

    vm.checkShowChilds = (nodeId) => {
      if (vm.childsShown[nodeId]) {
        return vm.childs[nodeId].every(child => child.parent === nodeId);
      }
      return false;
    };

  },

};