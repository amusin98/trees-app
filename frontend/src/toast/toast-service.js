const toast = {
  error: (toast, e) => {
    return toast.danger({content: `${e.status}: ${e.statusText}`});
  }
};

export default toast;