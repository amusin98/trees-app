import angular from 'angular';
import component from './node-metadata.component';

export default angular.module('nodeMetadataModule', [])
  .component('nodeMetadata', component)
  .name;