import template from './node-metadata.template.html';
import './node-metadata.less';

export default {
  bindings: {
    metadata: '<',
    changeMetadata: '&'
  },
  template,
  controller: function nodeMetadataController() {
    let vm = this;
    vm.$onChanges = function () {
      vm.textModel = '';
      if (vm.metadata) {
        vm.textModel = JSON.stringify(vm.metadata);
      }
    };
  },
};

